package com.epam.app;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class SortingTest {

  private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
  private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
  private final PrintStream originalOut = System.out;
  private final PrintStream originalErr = System.err;

  @Before
  public void setUpStreams() {
    System.setOut(new PrintStream(outContent));
    System.setErr(new PrintStream(errContent));
  }

  @After
  public void restoreStreams() {
    System.setOut(originalOut);
    System.setErr(originalErr);
  }

  @Test
  @Parameters(method = "sortingParams")
  public void testSorting(Integer[] source, String expected) {
    Sorting.main(source);
    assertEquals(expected, outContent.toString().trim());
  }

  public Object sortingParams() {
    return new Object[]{
        new Object[]{
            new Integer[]{3, 4, 2, 1, 5, -1},
            "[-1, 1, 2, 3, 4, 5]"
        }
    };
  }

  @Rule
  public ExpectedException expectedEx = ExpectedException.none();

  @Test
  public void testZeroElementsArrayCase() {
    expectedEx.expect(IllegalArgumentException.class);
    expectedEx.expectMessage("Input error");
    Integer[] arrayToSort = new Integer[]{};
    Sorting.main(arrayToSort);
  }

  @Test
  public void testNullArrayCase() {
    expectedEx.expect(IllegalArgumentException.class);
    expectedEx.expectMessage("Input error");
    Sorting.main(null);
  }

  @Test
  @Parameters({"1", "0", "-25"})
  public void testSingleElementArrayCase(Integer el) {
    Integer[] arrayToSort = {el};
    Sorting.main(arrayToSort);
    assertEquals(Arrays.toString(arrayToSort), outContent.toString().trim());
  }

  @Test
  @FileParameters(value = "src/test/resources/JunitParamsTenElementsTestParameters.csv", mapper = ArrayDataMapper.class)
  public void testTenElementsArrayCase(Integer[] source, Integer[] expected) {
    Sorting.main(source);
    assertEquals(Arrays.toString(expected), outContent.toString().trim());
  }

  @Test
  @FileParameters(value = "src/test/resources/JunitParamsMoreThanTenElementsTestParameters.csv", mapper = ArrayDataMapper.class)
  public void testMoreThanTenElementsArrayCase(Integer[] source, Integer[] ignored) {
    expectedEx.expect(IllegalArgumentException.class);
    expectedEx.expectMessage("Input error");
    Sorting.main(source);
  }

}

