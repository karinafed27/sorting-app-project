package com.epam.app;

import junitparams.mappers.CsvWithHeaderMapper;

import java.io.Reader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ArrayDataMapper extends CsvWithHeaderMapper {

  @Override
  public Object[] map(Reader reader) {
    final Object[] lines = super.map(reader);
    final List<Object[]> result = new LinkedList<>();

    for (Object line : lines) {
      final String[] parts = line.toString().split("\\|");
      result.add(new Object[]{
          convert(parts.length > 0 ? parts[0] : ""),
          convert(parts.length > 1 ? parts[1] : "")
      });
    }

    return result.toArray();
  }

  private Integer[] convert(String source) {
    final String[] nums = source.split(",");

    if (nums.length > 0) {
      return Arrays.stream(nums)
          .map(String::trim)
          .map(Integer::parseInt)
          .toArray(Integer[]::new);
    }

    return new Integer[]{};
  }
}

