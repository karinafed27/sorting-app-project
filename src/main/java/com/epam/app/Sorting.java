package com.epam.app;

import java.util.Arrays;

/**
 * The main class of the project
 */
public class Sorting {

  /**
   * Main method gets an array, sort it and outputs it after sorting
   */
  public static void main(Integer[] args) {
    if (args == null || args.length == 0 || args.length > 10) {
      throw new IllegalArgumentException("Input error");
    }
    Arrays.sort(args);
    System.out.println(Arrays.toString(args));
  }
}
