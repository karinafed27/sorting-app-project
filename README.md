# sorting-app-project
Complete the task to get a practical grasp of Apache Maven and its major features. 
You will need to create a Maven-based project – Sorting App. 
It is a small Java application that takes up to ten command-line arguments as integer values, sorts them in the ascending order, and then prints them into standard output.

Please read carefully and do the following:

Create a Maven project and specify its GAV settings, encoding, language level, etc.\
Write the code implementing the app specification.\
Configure the Maven project to build a runnable jar containing application and its dependencies.
Share the project using a public GitLab repository.\
Submit a link to your repository.

How this task will be evaluated:\
Please be aware that we expect the completed task to meet the following criteria:

-Maven Project of the model version 4.0.0 should be used.\
-Java 8 as a source and target version for the Compile stage should be used.\
-Maven properties in the configuration should be used.\
-A dependency should be introduced, for example, the Commons-IO library.
-Packages should be used.\
-Javadoc for a public API should be created.\
-Unit tests should be created with the help of JUnit 4.12.\
-Parametrized unit tests should be used.\
-Tests for the application should be created and corner cases should be covered. Corner cases are the cases with zero, one, ten, and more than 10 arguments. To know more about corner cases, please review an article.\
-A project should be configured to build a runnable jar.\
-Resource files should be used in the project (optional).